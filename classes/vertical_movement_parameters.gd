@tool
class_name VerticalMovementParameters
extends Resource

## Vertical acceleration due to gravity in pixels-per-second-per-second.
## Applied as long as vertical velocity is negative.
##
## [br][br]Gravity is applied whenever the body's [member PlatformerBody2D.state_vertical] is
## [member PlatformerBody2D.State.Vertical.Falling] or one of its descendants.
@export_range(0, 50000) var gravity_while_moving_up := 10000.0:
	set(value):
		gravity_while_moving_up = value
		changed.emit()

## Vertical acceleration due to gravity in pixels-per-second-per-second.
## Applied as long as vertical velocity is positive.
##
## [br][br]Gravity is applied whenever the body's [member PlatformerBody2D.state_vertical] is
## [member PlatformerBody2D.State.Vertical.Falling] or one of its descendants.
@export_range(0, 50000) var gravity_while_moving_down := 10000.0:
	set(value):
		gravity_while_moving_down = value
		changed.emit()

## Vertical speed of the jump in pixels-per-second (always negative).
##
## [br][br]This is the body's vertical speed as long as its
## [member PlatformerBody2D.state_vertical] is [member PlatformerBody2D.State.Vertical.JumpingUp].
@export_range(-10000, 0) var jump_speed := -1000.0:
	set(value):
		jump_speed = value
		changed.emit()

## Duration which the body spends traveling upwards at full [member jump_speed]
## in seconds.
##
## [br][br]The [member PlatformerBody2D.state_vertical] will be
## [member PlatformerBody2D.State.Vertical.JumpingUp] for this amount of time. When it
## runs out, the state changes to [member PlatformerBody2D.State.Vertical.Falling] and
## [member gravity_while_moving_up] takes over.
@export_range(0, 1, 0.01) var jump_duration := 0.2:
	set(value):
		jump_duration = value
		changed.emit()

## The maximum speed at which the body can fall (terminal velocity) in
## pixels-per-second.
@export_range(0, 10000) var fall_speed := 1000.0:
	set(value):
		fall_speed = value
		changed.emit()

## Duration in which the body can still perform a jump after starting to fall
## in seconds.
##
## [br][br]"Coyote Time" is the brief period after the body walks of a ledge
## where it can still perform a jump. This is to avoid punishing the player for
## input delay or for reacting just a couple frames too late. If used for a
## non-player character, it probably isn't much use. To see coyote time in
## action, set this to a high value and try jumping after walking off a ledge.
@export_range(0, 1, 0.01) var coyote_time: float = 0.05

## If true, the body will still fall during coyote time.
##
## [br][br]If false, gravity won't be applied until after coyote time expires.
##
## [br][br]For a more natural looking experience, avoiding the illusion of an
## invisible ledge, set this to true.
## [br]For an authentic coyote time experience, as seen on TV, set this to
## false. Side-effects may include loss of depth perception and flattening of
## the torso.
@export var apply_gravity_during_coyote_time: bool = true
