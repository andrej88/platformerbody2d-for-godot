@tool
class_name HorizontalMovementParameters
extends Resource

## Top speed of horizontal movement in pixels-per-second.
@export_range(0, 1000) var top_speed := 900.0:
	set(value):
		top_speed = value
		changed.emit()

## Acceleration of the body in pixels-per-second-per-second.
@export_range(0, 50000) var acceleration := 3000:
	set(value):
		acceleration = value
		changed.emit()

## Deceleration of the body in pixels-per-second-per-second (always negative).
@export_range(-50000, 0) var deceleration := -5000:
	set(value):
		deceleration = value
		changed.emit()
