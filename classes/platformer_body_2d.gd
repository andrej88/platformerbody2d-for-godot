## Specialization of CharacterBody2D designed for characters in platforming
## games.
##
## PlatformerBody2D extends [CharacterBody2D] by managing horizontal and
## vertical (jumping) movement in a state machine. The character is moved around
## by setting the member variables [member try_horizontal_movement_axis] and
## [member try_jump].
##
## [br][br]Its state can be queried through [member state_horizontal] and
## [member state_vertical]. State changes are emitted via the signals
## [signal horizontal_state_changed], [signal vertical_state_changed], and
## [signal facing_direction_changed].
##
## [br][br]The exported parameters configure how horizontal movement and jumping
## behave. In the editor, it previews the character's jump trajectory based on
## jumping-related parameters.
##
## [br][br]This body is designed assuming [member CharacterBody2D.motion_mode]
## is set to [code]MOTION_MODE_GROUNDED[/code].
##
## [br][br]Some notes for those reading the source code of this script:
## [br]Members that start with a double underscore are meant to be private,
## and those that start with [code]__tool[/code] are only relevant for when the
## script is running in the editor in [annotation @GDScript.@tool] mode.

@tool
@icon("platformer_body_2d.svg")
class_name PlatformerBody2D
extends CharacterBody2D

## Emitted when the body's [member state_horizontal] changes.
##
## [br][br]Its two parameters are the old and new state respectively.
signal horizontal_state_changed(from: State.Horizontal, to: State.Horizontal)

## Emitted when the body's [member state_vertical] changes.
##
## [br][br]Its two parameters are the old and new state respectively.
signal vertical_state_changed(from: State.Vertical, to: State.Vertical)

## Emitted when the body changes which horizontal direction it's facing. Its
## parameter indicates the new direction: 1 for right or -1 for left.
##
## [br][br]This signal is not necessarily accompanied by a
## [signal horizontal_state_changed]. For example, if the body is stuck between
## two walls and cannot move, this signal is emitted when it turns around but
## its state does not change. For updating the orientation of the body's sprite,
## this signal is more accurate.
signal facing_direction_changed(new_direction: int)


@export_group("Movement Parameters")

@export var default_grounded_horizontal_movement: HorizontalMovementParameters:
	set(value):
		default_grounded_horizontal_movement = value
		if (
			default_grounded_horizontal_movement != null
			and not default_grounded_horizontal_movement.changed.is_connected(__tool__redraw_jump_trajectory)
		):

			default_grounded_horizontal_movement.changed.connect(__tool__redraw_jump_trajectory)

@export var default_airborne_horizontal_movement: HorizontalMovementParameters:
	set(value):
		default_airborne_horizontal_movement = value
		if (
			default_airborne_horizontal_movement != null
			and not default_airborne_horizontal_movement.changed.is_connected(__tool__redraw_jump_trajectory)
		):
			default_airborne_horizontal_movement.changed.connect(__tool__redraw_jump_trajectory)

@export var default_vertical_movement: VerticalMovementParameters:
	set(value):
		default_vertical_movement = value
		if (
			default_vertical_movement != null
			and not default_vertical_movement.changed.is_connected(__tool__redraw_jump_trajectory)
		):
			default_vertical_movement.changed.connect(__tool__redraw_jump_trajectory)


var current_horizontal_movement: HorizontalMovementParameters
var current_vertical_movement: VerticalMovementParameters


@export_group("Platformer Collision")

## Collision layers that the body can collide with but are never considered
## walls.
##
## [br][br]For example, a layer containing only one-way platforms. The
## [b]inverse[/b] of these flags will be combined with this body's mask to
## compute the internal wall detectors' masks.
@export_flags_2d_physics var wall_detector_exceptions: int = 0


## The direction along the x-axis in which this body should attempt to move.
##
## [br][br]Positive numbers mean right, negative numbers mean left, and 0 means
## the body should slow to a halt. The magnitude of the value is ignored.
##
## [br][br]If the body can't move (e.g. because there's a wall in the way), it
## won't.
var try_horizontal_movement_axis: float = 0

## If true, the body will attempt to jump.
##
## If the body can't jump (e.g. because it's falling), it won't.
var try_jump: bool

## The current state of the body's horizontal movement.
##
## [br][br]See "Inherited by" list in [PlatformerBody2D.State.Horizontal] for
## possible values.
var state_horizontal: State.Horizontal = State.Horizontal.Still.new(self):
	set(value):
		var old_state_horizontal := state_horizontal
		state_horizontal = value
		if state_horizontal != old_state_horizontal:
			horizontal_state_changed.emit(old_state_horizontal, state_horizontal)

## The current state of the body's horizontal movement.
##
## [br][br]See "Inherited by" list in [PlatformerBody2D.State.Vertical] for
## possible values.
var state_vertical: State.Vertical = State.Vertical.Grounded.new(self):
	set(value):
		var old_state_vertical := state_vertical
		state_vertical = value
		if state_vertical != old_state_vertical:
			vertical_state_changed.emit(old_state_vertical, state_vertical)


## The direction the body is currently facing.
var facing_direction: int:
	set(value):
		var old_facing_direction := facing_direction
		facing_direction = value
		if facing_direction != old_facing_direction:
			facing_direction_changed.emit(facing_direction)


var wall_direction: int:
	get:
		if not __left_walls.is_empty() and __right_walls.is_empty():
			return -1
		elif __left_walls.is_empty() and not __right_walls.is_empty():
			return 1
		else:
			return 0


# The following member variables are "private"

# These are used as sets rather than dictionaries
var __left_walls: Dictionary
var __right_walls: Dictionary

# The internal node used in the editor to render the character's jump trajectory.
var __tool__jump_trajectory: __Tool_JumpTrajectory


func _init():
	if Engine.is_editor_hint():
		if (
			default_grounded_horizontal_movement != null
			and not default_grounded_horizontal_movement.changed.is_connected(__tool__redraw_jump_trajectory)
		):
			default_grounded_horizontal_movement.changed.connect(__tool__redraw_jump_trajectory)

		if (
			default_airborne_horizontal_movement != null
			and not default_airborne_horizontal_movement.changed.is_connected(__tool__redraw_jump_trajectory)
		):
			default_airborne_horizontal_movement.changed.connect(__tool__redraw_jump_trajectory)

		if (
			default_vertical_movement != null
			and not default_vertical_movement.changed.is_connected(__tool__redraw_jump_trajectory)
		):
			default_vertical_movement.changed.connect(__tool__redraw_jump_trajectory)


func _ready() -> void:

	current_horizontal_movement = default_grounded_horizontal_movement
	current_vertical_movement = default_vertical_movement

	# Add wall detectors. is_on_wall() seems to be a bit unreliable.
	# The collision mask is this body's collision mask minus the exceptions in wall_detector_exceptions
	# Multiply scale.y by 0.9 to avoid false-positive detection with floors and slopes.
	__add_detector(Vector2(-2, 0), Vector2(1, 0.9), __left_walls, self.collision_mask & ~wall_detector_exceptions)
	__add_detector(Vector2(2, 0), Vector2(1, 0.9), __right_walls, self.collision_mask & ~wall_detector_exceptions)

	# Draw the jump trajectory in the editor
	if Engine.is_editor_hint():
		__tool__jump_trajectory = __Tool_JumpTrajectory.new(self, Color(1, 1, 0, 0.5))
		__tool__jump_trajectory.body = self
		add_child(__tool__jump_trajectory, false, INTERNAL_MODE_BACK)


func _physics_process(delta: float) -> void:

	if Engine.is_editor_hint():
		return

	# Step 1: Update the state
	state_horizontal = state_horizontal._update()
	state_vertical = state_vertical._update()

	# Step 2: Handle physics per-state
	state_horizontal._physics_process(delta)
	state_vertical._physics_process(delta)

	# Step 3: Move and slide
	move_and_slide()


# Adds an Area2D node as an internal child. Its collider is the same as this
# body's, but offset and scaled according to those parameters. Its collision
# mask is taken from the parameter. Its body_entered and body_exited signals
# are connected to a lambda that inserts or removes the other body from
# collision_store.
func __add_detector(offset: Vector2, scale_multiplier: Vector2, collision_store: Dictionary, detector_collision_mask: int) -> void:

	var own_collider: Node2D
	for child in get_children():
		if child is CollisionShape2D or child is CollisionPolygon2D:
			own_collider = child
			break

	if own_collider == null:
		return

	var collider = own_collider.duplicate()
	collider.position += offset
	collider.scale *= scale_multiplier
	var detector_area = Area2D.new()
	detector_area.collision_layer = 0
	detector_area.collision_mask = detector_collision_mask
	detector_area.add_child(collider)

	detector_area.body_entered.connect(
		func(body: Node2D):
			if body != self:				# Ignore collisions with this PlatformerBody2D
				collision_store[body] = true
	)

	detector_area.body_exited.connect(
		func(body: Node2D):
			collision_store.erase(body)
	)

	add_child(detector_area, false, INTERNAL_MODE_BACK)


func __tool__redraw_jump_trajectory():
	if Engine.is_editor_hint() and __tool__jump_trajectory != null:
		__tool__jump_trajectory.queue_redraw()


## An "abstract" State class used as a base for other State classes.
class State:

	var body: PlatformerBody2D

	func _init(p_body: PlatformerBody2D):
		self.body = p_body

	func _get_name() -> StringName:
		printerr("The current state either did not implement _get_name(), or is not supposed to be instantiated directly.")
		return ""

	## Determines whether the it's time for the state to change.
	## Returns [code]self[/code] if no state change needs to take place.
	func _update():
		return self

	func _physics_process(_delta: float) -> void: pass

	## The base of all State classes describing horizontal movement.
	class Horizontal extends State:

		func _update() -> State.Horizontal:
			return self

		class Still extends Horizontal:
			func _get_name() -> StringName: return "Still"

			func _update() -> State.Horizontal:
				if body.try_horizontal_movement_axis < 0:
					body.facing_direction = -1
					if body.__left_walls.is_empty():
						return State.Horizontal.Left.new(body)
					else:
						return self
				elif body.try_horizontal_movement_axis > 0:
					body.facing_direction = 1
					if body.__right_walls.is_empty():
						return State.Horizontal.Right.new(body)
					else:
						return self
				else:
					return self

			func _physics_process(delta: float) -> void:
				# deceleration is in px/s/s, so multiplying by delta gives px/s, the same units as `velocity`
				if body.velocity.x > 0:
					var vx = body.velocity.x + body.current_horizontal_movement.deceleration * delta
					body.velocity.x = clampf(vx, 0, body.current_horizontal_movement.top_speed)
				elif body.velocity.x < 0:
					var vx = body.velocity.x - body.current_horizontal_movement.deceleration * delta
					body.velocity.x = clampf(vx, -body.current_horizontal_movement.top_speed, 0)


		class Left extends Horizontal:
			func _get_name() -> StringName: return "Left"

			func _update() -> State.Horizontal:
				if body.try_horizontal_movement_axis < 0 and body.__left_walls.is_empty():
					return self
				elif body.try_horizontal_movement_axis > 0:
					body.facing_direction = 1
					return State.Horizontal.Right.new(body)
				else:
					return State.Horizontal.Still.new(body)

			func _physics_process(delta: float) -> void:
				# acceleration is in px/s/s, so multiplying by delta gives px/s, the same units as `velocity`
				var vx: float = body.velocity.x
				if vx > 0:
					body.velocity.x += body.current_horizontal_movement.deceleration * delta
				elif vx < -body.current_horizontal_movement.top_speed:
					body.velocity.x = minf(vx - body.current_horizontal_movement.deceleration * delta, -body.current_horizontal_movement.top_speed)
				elif vx != -body.current_horizontal_movement.top_speed:
					body.velocity.x = maxf(vx - body.current_horizontal_movement.acceleration * delta, -body.current_horizontal_movement.top_speed)


		class Right extends Horizontal:
			func _get_name() -> StringName: return "Right"

			func _update() -> State.Horizontal:
				if body.try_horizontal_movement_axis < 0:
					body.facing_direction = -1
					return State.Horizontal.Left.new(body)
				elif body.try_horizontal_movement_axis > 0 and body.__right_walls.is_empty():
					return self
				else:
					return State.Horizontal.Still.new(body)

			func _physics_process(delta: float) -> void:
				# acceleration is in px/s/s, so multiplying by delta gives px/s, the same units as `velocity`
				var vx: float = body.velocity.x
				if vx < 0:
					body.velocity.x -= body.current_horizontal_movement.deceleration * delta
				elif vx > body.current_horizontal_movement.top_speed:
					body.velocity.x = maxf(vx + body.current_horizontal_movement.deceleration * delta, body.current_horizontal_movement.top_speed)
				elif vx != body.current_horizontal_movement.top_speed:
					body.velocity.x = minf(vx + body.current_horizontal_movement.acceleration * delta, body.current_horizontal_movement.top_speed)


	## The base of all State classes describing vertical movement.
	class Vertical extends State:

		func _update() -> State.Vertical:
			return self

		class Grounded extends Vertical:
			func _get_name() -> StringName: return "Grounded"

			func _init(p_body: PlatformerBody2D):
				super(p_body)
				p_body.current_horizontal_movement = p_body.default_grounded_horizontal_movement

			func _update() -> State.Vertical:
				if not body.is_on_floor():
					if body.current_vertical_movement.coyote_time > 0:
						return State.Vertical.Airborne.Falling.CoyoteTime.new(body)
					else:
						return State.Vertical.Airborne.Falling.new(body)

				elif body.try_jump:
						return State.Vertical.Airborne.JumpingUp.new(body)

				else:
					return self


		class Airborne extends Vertical:

			func _init(p_body: PlatformerBody2D):
				super(p_body)
				p_body.current_horizontal_movement = p_body.default_airborne_horizontal_movement

			class JumpingUp extends Airborne:
				func _get_name() -> StringName: return "JumpingUp"

				var jump_duration: float
				var jump_time: float = 0

				func _init(p_body: PlatformerBody2D):
					super(p_body)
					self.jump_duration = body.current_vertical_movement.jump_duration

				func _update():
					if (
						jump_time >= jump_duration
						or not body.try_jump
						or (body.is_on_ceiling() and not body.is_on_floor())
					):
						return State.Vertical.Airborne.Falling.new(body)
					else:
						return self

				func _physics_process(delta: float) -> void:
					body.velocity.y = body.current_vertical_movement.jump_speed
					jump_time += delta


			class Falling extends Airborne:
				func _get_name() -> StringName: return "Falling"

				func _update() -> State.Vertical:
					if body.is_on_floor():
						return State.Vertical.Grounded.new(body)
					else:
						return self

				func _physics_process(delta: float) -> void:
					# gravity is in px/s/s, so multiplying by delta gives px/s, the same units as `velocity`
					var gravity: float
					if body.velocity.y < 0:
						gravity = body.current_vertical_movement.gravity_while_moving_up
					else:
						gravity = body.current_vertical_movement.gravity_while_moving_down

					var vy: float = body.velocity.y + gravity * delta
					body.velocity.y = clampf(vy, body.current_vertical_movement.jump_speed, body.current_vertical_movement.fall_speed)


				class CoyoteTime extends Falling:
					func _get_name() -> StringName: return "CoyoteTime"

					var total_coyote_time: float
					var elapsed_time := 0.0

					func _update():
						var new_state := super._update()
						if new_state != self:
							return new_state

						elif elapsed_time >= body.current_vertical_movement.coyote_time:
							return State.Vertical.Airborne.Falling.new(body)

						elif body.try_jump:
								return State.Vertical.Airborne.JumpingUp.new(body)

						else:
							return self


					func _physics_process(delta: float):
						elapsed_time += delta

						if body.current_vertical_movement.apply_gravity_during_coyote_time:
							super._physics_process(delta)


class __Tool_JumpTrajectory extends Node2D:

	var physics_ticks := ProjectSettings.get_setting("physics/common/physics_ticks_per_second") as int
	var physics_delta := 1.0 / physics_ticks
	var body: PlatformerBody2D
	var trajectory_color: Color
	var point_color: Color

	const points_per_tick := 4

	func _init(p_body: PlatformerBody2D, p_trajectory_color: Color):
		body = p_body
		trajectory_color = p_trajectory_color


	func _draw() -> void:
		if (
			body == null
			or body.default_grounded_horizontal_movement == null
			or body.default_airborne_horizontal_movement == null
			or body.default_vertical_movement == null
		):
			return

		var points: Array[Vector2] = []
		var curve: Array[Vector2] = []
		var simulated_jump_time := 0.0
		var simulated_velocity_y := body.default_vertical_movement.jump_speed
		var simulated_velocity_x := body.default_grounded_horizontal_movement.top_speed
		var simulated_position := Vector2(0, 0)
		points.append(simulated_position)
		curve.append(simulated_position)

		for time_s in range(0, 10):
			var should_break := false

			for tick in range(0, physics_ticks):

				for subtick in range(0, points_per_tick):

					simulated_jump_time += physics_delta / points_per_tick

					if simulated_jump_time > body.default_vertical_movement.jump_duration:
						var gravity: float
						if simulated_velocity_y < 0:
							gravity = body.default_vertical_movement.gravity_while_moving_up
						else:
							gravity = body.default_vertical_movement.gravity_while_moving_down

						simulated_velocity_y += gravity * physics_delta / points_per_tick
						simulated_velocity_y = clamp(simulated_velocity_y, body.default_vertical_movement.jump_speed, body.default_vertical_movement.fall_speed)

					if body.default_airborne_horizontal_movement.top_speed > body.default_grounded_horizontal_movement.top_speed:
						simulated_velocity_x = minf(
							simulated_velocity_x + body.default_airborne_horizontal_movement.acceleration * physics_delta / points_per_tick,
							body.default_airborne_horizontal_movement.top_speed
						)
					else:
						simulated_velocity_x = maxf(
							simulated_velocity_x + body.default_airborne_horizontal_movement.deceleration * physics_delta / points_per_tick,
							body.default_airborne_horizontal_movement.top_speed
						)

					simulated_position.x += simulated_velocity_x * physics_delta / points_per_tick
					simulated_position.y += simulated_velocity_y * physics_delta / points_per_tick

					curve.append(simulated_position)

				points.append(simulated_position)

				if simulated_position.y >= -0.01:
					should_break = true
					break

				if should_break: break

			if should_break: break

		draw_polyline(curve, trajectory_color)

		for point in points:
			draw_circle(point, 1, trajectory_color)
