- Figure out how to apply velocity after leaving a conveyor. Should be conveyor
  velocity + move_speed but is actually just move_speed.
- Split deceleration into two parameters:
	- One applied when going from too-fast back to down to top-speed.
	- One applied when switching directions or halting.
		- Possibly split those two as well. I think New Super Mario Bros works that way
		  where letting go of the button means mario gradually slows, but pressing the
		  opposite direction is a much snappier change. I think. Memory is fuzzy.
- Fix trying to fall through a sloped one-way platform while moving downhill
	- Currently it detects that the y position increased but the body did not pass through
	  the one-way collider. One solution could be to have a detector at the top of the body
	  which, upon receiving body_entered, updates the state.
