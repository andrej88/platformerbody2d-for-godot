@tool
extends PlatformerBody2D

## This script processes input and contains signal callbacks.
##
## Some typical platformer behavior is defined in this script instead of in
## the PlatformerBody2D class directly. The reasoning behind this is that
## PlatformerBody2D is, after all, an extension of CharacterBody2D and therefore
## a physics node, first and foremost. Mixing input or rendering logic into a
## node ostensbily responsible only for physics has several downsides. First, it
## violates the user's expectations of what the node does. Second, it makes the
## implementation more complex and harder to modify should the user want to
## customize it. Finally, it would make it harder to use the node for characters
## which are not controlled by the player (enemies, NPCs, whatever).
##
## Some typical platformer functionality that you need to define yourself:
## - Interpreting inputs as movements
## - Updating the sprite based on the body's state
## - Auto-jump (or lack thereof)
## - Jump buffering (pressing jump just before landing should still register
##   even when auto-jump is off)
## - Dropping back down through one-way platforms

## If the jump action is pressed this many seconds before landing, the player
## will jump as if the button was pressed normally.
@export_range(0, 1, 0.01) var jump_buffer_duration := 0.1

@export_range(0, 2, 0.1) var jump_cancellation_strength := 1.0

## These layers will briefly be ignored when the player presses crouch+jump
@export_flags_2d_physics var one_way_platform_layers := 0


@onready var visual_node := $Visual as Node2D


var pre_landing_jump_time: int
var __one_way_platforms: Dictionary


func _unhandled_key_input(event: InputEvent) -> void:

	if event.is_action_pressed("jump"):
		get_viewport().set_input_as_handled()
		if not event.is_echo():
			if self.state_vertical is State.Vertical.Airborne.Falling:
				pre_landing_jump_time = Time.get_ticks_msec()

			else:
				if Input.is_action_pressed("crouch") and self.one_way_platform_layers != 0:
						# If there is a one-way platform below, then fall through
						if not self.__one_way_platforms.is_empty():
							self.collision_mask = self.collision_mask & ~self.one_way_platform_layers
							self.state_vertical = State.Vertical.Airborne.Falling.new(self)

				else:
					self.try_jump = true

	elif event.is_action_released("jump"):
		get_viewport().set_input_as_handled()

		if self.state_vertical is State.Vertical.Airborne:
			self.velocity.y = minf(self.velocity.y + (self.current_vertical_movement.fall_speed * jump_cancellation_strength), self.current_vertical_movement.fall_speed)

		self.try_jump = false

	elif event.is_action("move_left") or event.is_action("move_right"):
		get_viewport().set_input_as_handled()
		self.try_horizontal_movement_axis = Input.get_axis("move_left", "move_right")


func _on_player_horizontal_state_changed(_from: State.Horizontal, to: State.Horizontal) -> void:
	# This is where you would normally update the sprite. Since there is no
	# sprite, I'm using the Polygon's `skew` property to visually indicate that
	# it's moving, and the scale in place of `Sprite2D.flip_h`. The skew makes
	# it appear like it's not touching the ground but for the sake of example I
	# can live with that.

	if to is State.Horizontal.Left:
		visual_node.skew = -PI / 18	# -15°
	elif to is State.Horizontal.Right:
		visual_node.skew = PI / 18		# 15°
	else:
		if not to is State.Horizontal.Still:
			printerr("Unexpected state: %s" % to._get_name())

		visual_node.skew = 0


func _on_player_vertical_state_changed(from: State.Vertical, to: State.Vertical) -> void:
	if from is State.Vertical.Airborne.JumpingUp:
		# Disable auto-jump
		self.try_jump = false

	if to is State.Vertical.Grounded:
		var current_time := Time.get_ticks_msec()
		var seconds_since_jump_pressed := (current_time - pre_landing_jump_time) / 1000.0
		if seconds_since_jump_pressed < jump_buffer_duration:
			if Input.is_action_pressed("crouch") and self.one_way_platform_layers != 0:
				# If there is a one-way platform below, then fall through
				if not self.__one_way_platforms.is_empty():
					self.collision_mask = self.collision_mask & ~self.one_way_platform_layers
					self.state_vertical = State.Vertical.Airborne.Falling.new(self)

			else:
				self.try_jump = true


func _on_player_facing_direction_changed(new_direction: int) -> void:
	visual_node.scale.x = new_direction


func _on_one_way_platform_detector_body_entered(body: Node2D) -> void:
	if body != self:
		__one_way_platforms[body] = true


func _on_one_way_platform_detector_body_exited(body: Node2D) -> void:
	__one_way_platforms.erase(body)
	self.collision_mask = self.collision_mask | self.one_way_platform_layers
