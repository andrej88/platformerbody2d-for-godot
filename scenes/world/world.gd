extends Node2D


var __show_collision_shapes := false


func _ready() -> void:
	$"Zones/Playground/Moving Platforms/UpperPlatform/AnimationPlayer".play("BackAndForth")
	$"Zones/Playground/Moving Platforms/LowerPlatform/AnimationPlayer".play("BackAndForth")
	$"Zones/Playground/Conveyor/StaticBody2D/AnimationPlayer".play("RotateAxles")
	$Zones/Wheels/Wheels/Wheel1/AnimationPlayer.play("spin")
	$Zones/Wheels/Wheels/Wheel2/AnimationPlayer.play("spin")
	$Zones/Wheels/Wheels/Wheel3/AnimationPlayer.play("spin")
	$Zones/Wheels/Wheels/Wheel4/AnimationPlayer.play("spin")


func _unhandled_input(event: InputEvent) -> void:

	if event.is_action_pressed("toggle_collision_shapes") and not event.is_echo():
		get_viewport().set_input_as_handled()

		get_tree().root.set_input_as_handled()

		__show_collision_shapes = not __show_collision_shapes

		if __show_collision_shapes:
			get_tree().debug_collisions_hint = true
		else:
			get_tree().debug_collisions_hint = false

		queue_redraw_collision_stuff()


	elif event.is_action_pressed("quit") and not event.is_echo():
		get_viewport().set_input_as_handled()
		get_tree().quit()


	elif event.is_action_pressed("toggle_fullscreen") and not event.is_echo():
		get_viewport().set_input_as_handled()

		if get_window().mode == Window.MODE_EXCLUSIVE_FULLSCREEN or get_window().mode == Window.MODE_FULLSCREEN:
			get_window().mode = Window.MODE_WINDOWED
		else:
			get_window().mode = Window.MODE_EXCLUSIVE_FULLSCREEN

	elif event is InputEventMouseButton:
		var mouse_event := event as InputEventMouseButton
		if mouse_event.button_index == MOUSE_BUTTON_LEFT and mouse_event.pressed:
			get_viewport().set_input_as_handled()
			print(mouse_event.position)
			print(mouse_event.global_position)

			$Player.global_position = mouse_event.position + $Camera2D.global_position - Vector2(
				ProjectSettings.get_setting("display/window/size/viewport_width") / 2.0,
				ProjectSettings.get_setting("display/window/size/viewport_height") / 2.0,
			)


func _on_playground_body_entered(body: Node2D) -> void:
	if body == $Player:
		$Camera2D.global_position = $Zones/Playground/CameraZone/CollisionShape2D.global_position


func _on_platforms_body_entered(body: Node2D) -> void:
	if body == $Player:
		$Camera2D.global_position = $Zones/Platforms/CameraZone/CollisionShape2D.global_position


func _on_grassland_body_entered(body: Node2D) -> void:
	if body == $Player:
		$Camera2D.global_position = $Zones/Grassland/CameraZone/CollisionShape2D.global_position


func _on_wheels_body_entered(body: Node2D) -> void:
	if body == $Player:
		$Camera2D.global_position = $Zones/Wheels/CameraZone/CollisionShape2D.global_position


## Traverse entire scene tree and call queue_redraw on all instances of
## CollisionShape2D and CollisionPolygon2D.
## Adapted from:
## https://github.com/godotengine/godot-proposals/issues/2072#issuecomment-881332386
func queue_redraw_collision_stuff():
	var node_stack: Array[Node] = [get_tree().get_root()]
	while not node_stack.is_empty():
		var node: Node = node_stack.pop_back()
		if is_instance_valid(node):
			if (node is CollisionShape2D or node is CollisionPolygon2D) and node.get_parent().name != "CameraZone":
				node.queue_redraw()
			node_stack.append_array(node.get_children(true))

