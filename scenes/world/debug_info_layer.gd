extends CanvasLayer


@onready var body := $"../Player" as PlatformerBody2D


func _process(_delta: float) -> void:
	%LabelHorizontalAxis.text = "try_horizontal_movement_axis: %+d" % body.try_horizontal_movement_axis
	%LabelTryJump.text = "try_jump: %5s" % body.try_jump

	%LabelVelocity.text = "velocity: [%+5d, %+5d]" % [body.velocity.x, body.velocity.y]
	%LabelIsOnFloor.text = "is_on_floor(): %s" % body.is_on_floor()
	%LabelIsOnWall.text = "is_on_wall(): %s" % body.is_on_wall()
	@warning_ignore("incompatible_ternary")
	%LabelWallDirection.text = "-get_wall_normal().x: %3s" % (-body.get_wall_normal().x if body.is_on_wall() else "n/a")

	%LabelStateHorizontal.text = "state_horizontal: %s" % body.state_horizontal._get_name()
	%LabelStateVertical.text = "state_vertical: %s" % body.state_vertical._get_name()
	%LabelCustomWallDetection.text = "Custom Wall Detection: "

	if not body.__left_walls.is_empty() and not body.__right_walls.is_empty():
		%LabelCustomWallDetection.text += "Left & Right"
	elif not body.__left_walls.is_empty():
		%LabelCustomWallDetection.text += "Left"
	elif not body.__right_walls.is_empty():
		%LabelCustomWallDetection.text += "Right"
	else:
		%LabelCustomWallDetection.text += "None"
