extends Node2D

@export var trail_length_in_frames := 120

var trails: PackedVector2Array
var start_idx: int = 0

@onready var player := $"../Player" as PlatformerBody2D

func _physics_process(_delta: float) -> void:
	if trails.size() < trail_length_in_frames:
		trails.append(player.global_position)
	else:
		trails[start_idx] = player.global_position

	start_idx += 1
	start_idx = start_idx % trail_length_in_frames

	queue_redraw()


func _unhandled_input(event: InputEvent) -> void:
	if event.is_action_pressed("toggle_trails") and not event.is_echo():
		get_viewport().set_input_as_handled()
		visible = not visible


func _draw() -> void:

	var to_draw_in_order := trails.slice(start_idx)
	to_draw_in_order.append_array(trails.slice(0, start_idx))

	for point in trails:
		draw_circle(point, 1.0, Color(1, 1, 0, 1))

	if to_draw_in_order.size() >= 2:
		draw_polyline(to_draw_in_order, Color(1, 1, 0, 0.5))
