# <img src="./icon.svg" style="height: 1em; margin-bottom: -0.15em" /> PlatformerBody2D

PlatformerBody2D is a custom class for Godot 4 which extends the built-in CharacterBody2D class with functionality to ease the development of a character in a 2D platformer.

The class is concerned only with the physics of the body, while input handling, sprite updates, and other non-physics behavior need to be implemented separately.

The `classes` folder contains all that is needed to integrate PlatformerBody2D into your project. When installing from the asset store, check only the files in `classes` and place them wherever you see fit.

The rest of this repository, including `project.godot` and the `scenes` folder, are a demo showing an example of how PlatformerBody2D may be used effectively.


## Installation

TODO once published on the asset store.

## Quick Start

After installing the necessary files to your project, PlatformerBody2D is ready to be used.

Create a new scene with PlatformerBody2D as the root node, and add sprites and anythign else you want this character to contain.

Select the root node and take a look at the inspector. Above the usual CharacterBody2D properties, there is a section for PlatformerBody2D properties. The first group here is where you defined movement parameters like movement speed, gravity, and jump speed. The second group allows 
